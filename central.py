import binascii
import struct
import time
from bluepy import btle
from bluepy.btle import UUID, Peripheral
from jwcrypto import jwk, jws
import time
import json
from database import Database
import datetime
import threading
import sys
from tokenUtils import *
from remoteDB import *

# Evita Broken pipe error
# from signal import signal, SIGPIPE, SIG_DFL
# signal(SIGPIPE,SIG_DFL)

def appendToLog(text):
    print(text)
    # with open('/home/pi/centralBLE/log.txt', 'a') as fh:
    #     fh.write("[{date}]: {text}\n".format(date=datetime.datetime.now(), text=text))


class MyDelegate(btle.DefaultDelegate):
    p = None

    def __init__(self, peripheral):
        btle.DefaultDelegate.__init__(self)
        self.p = peripheral

    def handleNotification(self, cHandle, data):

        char = self.p.readCharacteristic(cHandle)
        if (len(sys.argv) > 1 and sys.argv[1] == "async"):
            token = char.decode("ASCII")
            bd.insertJWS(token)
        else:
            jwstoken = decodeToken(char)
            verifyToken(jwstoken)
            appendToLog(jwstoken.payload)
            data = json.loads(jwstoken.payload)

            header = jwstoken.jose_header
            beaconId = header.get("beaconId")
            bd_beaconId = bd.getBeaconId(beaconId)

            if bd_beaconId > 0:
                if data.get("uptime") is not None:
                    # bd.insertStatusData(bd_beaconId, data.get("uptime"), data.get("rssi"))
                    insertStatusData(bd_beaconId, data)
                if data.get("gyroX") is not None:
                    # bd.insertGyroData(bd_beaconId, data.get("gyroX"), data.get("gyroY"), data.get("gyroZ"))
                    insertGyroData(bd_beaconId, data)
                if data.get("accelX") is not None:
                    # bd.insertAccelData(bd_beaconId, data.get("accelX"), data.get("accelY"), data.get("accelZ"))
                    insertAccelData(bd_beaconId, data)


def connectBeacon(beacon):
    password = b"123456"

    appendToLog("Thread: " + beacon[0])

    p = Peripheral(beacon[0], beacon[1])

    p.getServices()
    dataService = p.getServiceByUUID("00007777-0000-1000-8000-00805f9b34fb")

    # Auth Characteristics
    authChar = dataService.getCharacteristics(UUID("0000"))[0]
    authStatusChar = dataService.getCharacteristics(UUID("0001"))[0]

    # Data Characteristics
    accelChar = dataService.getCharacteristics(UUID("6666"))[0]
    gyroChar = dataService.getCharacteristics(UUID("5555"))[0]
    statusChar = dataService.getCharacteristics(UUID("4444"))[0]

    # Activant notificacions de les Characteristics
    p.setDelegate(MyDelegate(p))
    p.writeCharacteristic(accelChar.valHandle + 1, b"\x01\x00")
    p.writeCharacteristic(gyroChar.valHandle + 1, b"\x01\x00")
    p.writeCharacteristic(statusChar.valHandle + 1, b"\x01\x00")

    # Authenticating
    try:
        appendToLog(p.addr + " " + str(authStatusChar.read()))
        if authStatusChar.read() == b'\x00':
            appendToLog("Authenticating: " + p.addr)
            authChar.write(password, True)
    except Exception as e:
        appendToLog(e)

    # Esperant notificacions
    try:
        while True:
            if p.waitForNotifications(1.0):
                # handleNotification() was called
                appendToLog("Notification: " + p.addr)
                continue
            appendToLog("Waiting... " + p.addr)

    except Exception as e:
        appendToLog("inside: " + p.addr)
        appendToLog(e)

bd = Database()
beacons = bd.getBeacons()

if (beacons == -1):
    appendToLog("NO BEACONS ENABLED")
    quit()

threadsArray = []

while True:
    try:
        appendToLog("START")

        if (len(threadsArray) == 0):
            for beacon in beacons:
                t = threading.Thread(target=connectBeacon, args=(beacon,))
                t.setName(beacon[0])
                t.start()
                threadsArray.append(t)
        else:
            for index in range(len(threadsArray)):
                if threadsArray[index].isAlive():
                    print("Alive thread: " + threadsArray[index].getName())
                    continue
                print("Restarting thread: " + threadsArray[index].getName())
                threadsArray[index] = threading.Thread(target=connectBeacon, args=(beacons[index],))
                threadsArray[index].setName(beacons[index][0])
                threadsArray[index].start()


    except Exception as e:
        appendToLog("outside")
        appendToLog(e)

    appendToLog("END")
    time.sleep(5.0)

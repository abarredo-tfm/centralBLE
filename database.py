import mysql.connector
import datetime

class Database:
  def __init__(self):
    self.mydb = mysql.connector.connect(
      host="localhost",
      user="",
      password="",
      database=""
    )

  def insertGyroData(self, beacon_id, x, y, z):
    mycursor = self.mydb.cursor()

    sql = "INSERT INTO gyro_data (beacon_id, x, y, z, created_at) VALUES (%s, %s, %s, %s, %s)"

    created_at = datetime.datetime.now()
    val = (beacon_id, x, y, z, created_at)
    mycursor.execute(sql, val)

    self.mydb.commit()

  def insertAccelData(self, beacon_id, x, y, z):
    mycursor = self.mydb.cursor()

    sql = "INSERT INTO accel_data (beacon_id, x, y, z, created_at) VALUES (%s, %s, %s, %s, %s)"

    created_at = datetime.datetime.now()
    val = (beacon_id, x, y, z, created_at)
    mycursor.execute(sql, val)

    self.mydb.commit()

  def insertStatusData(self, beacon_id, uptime, rssi):
    mycursor = self.mydb.cursor()

    sql = "INSERT INTO status_data (beacon_id, uptime, rssi, created_at) VALUES (%s, %s, %s, %s)"

    created_at = datetime.datetime.now()
    val = (beacon_id, uptime, rssi, created_at)
    mycursor.execute(sql, val)

    self.mydb.commit()

  def getBeaconId(self, name):
    mycursor = self.mydb.cursor()

    sql = "SELECT id FROM beacon WHERE name = %s"

    val = (name, )
    mycursor.execute(sql, val)

    myresult = mycursor.fetchone()

    if(mycursor.rowcount > 0):
        return myresult[0]

    return -1

  def getBeacons(self):
    mycursor = self.mydb.cursor()

    sql = "SELECT address, address_type FROM beacon WHERE is_enabled = 1"

    mycursor.execute(sql)

    myresult = mycursor.fetchall()

    if(mycursor.rowcount > 0):
        return myresult

    return -1

  def insertJWS(self, data):
    mycursor = self.mydb.cursor()

    sql = "INSERT INTO jws (token, created_at) VALUES (%s, %s)"

    created_at = datetime.datetime.now()
    val = (data, created_at)
    mycursor.execute(sql, val)

    self.mydb.commit()

  def getUnprocessedJWS(self):
    mycursor = self.mydb.cursor()

    sql = "SELECT * FROM jws WHERE processed = 0 ORDER BY id ASC LIMIT 1"

    mycursor.execute(sql)

    myresult = mycursor.fetchone()

    if (mycursor.rowcount > 0):
      return myresult

    return -1

  def markJWSAsProcessed(self, jws_id):
    mycursor = self.mydb.cursor()

    sql = "UPDATE jws SET processed = 1 WHERE id = %s"

    mycursor.execute(sql, (jws_id,))

    self.mydb.commit()
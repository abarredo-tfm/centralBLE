from database import Database
import json
import time

from tokenUtils import *

bd = Database()
jws = bd.getUnprocessedJWS()
while True:
    try:
        jwstoken = decodeToken(jws[1])
        verifyToken(jwstoken)
        data = json.loads(jwstoken.payload)

        header = jwstoken.jose_header
        beaconId = header.get("beaconId")
        bd_beaconId = bd.getBeaconId(beaconId)

        if bd_beaconId > 0:
            if data.get("uptime") is not None:
                bd.insertStatusData(bd_beaconId, data.get("uptime"), data.get("rssi"))
            if data.get("gyroX") is not None:
                bd.insertGyroData(bd_beaconId, data.get("gyroX"), data.get("gyroY"), data.get("gyroZ"))
            if data.get("accelX") is not None:
                bd.insertAccelData(bd_beaconId, data.get("accelX"), data.get("accelY"), data.get("accelZ"))


    except Exception as e:
        appendToLog(e)

    bd.markJWSAsProcessed(jws[0])
    jws = bd.getUnprocessedJWS()
    time.sleep(1000)

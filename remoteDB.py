import requests

url = 'https://localhost/api/insert/'

def insertGyroData(bd_beaconId, data):
    data["beacon"] = bd_beaconId
    x= requests.post(url+"gyro", data=data, verify=False)

def insertAccelData(bd_beaconId, data):
    data["beacon"] = bd_beaconId
    x = requests.post(url + "accel", data=data, verify=False)

def insertStatusData(bd_beaconId, data):
    data["beacon"] = bd_beaconId
    x = requests.post(url + "status", data=data, verify=False)
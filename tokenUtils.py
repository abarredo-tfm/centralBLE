from jwcrypto import jwk, jws

def appendToLog(text):
    print(text)
    # with open('/home/pi/centralBLE/log.txt', 'a') as fh:
    #     fh.write("[{date}]: {text}\n".format(date=datetime.datetime.now(), text=text))

def decodeToken(token):
    try:
        jwstoken = jws.JWS()

        if(type(token) != str):
            token = token.decode("ASCII")

        jwstoken.deserialize(token)
        return jwstoken
    except Exception as e:
        appendToLog(e)
        appendToLog("Formato invalido")


def verifyToken(jwstoken):
    try:
        header = jwstoken.jose_header
        beaconId = header.get("beaconId")
        with open("certs/" + beaconId + ".pem", "rb") as pemfile:
            key = jwk.JWK.from_pem(pemfile.read())
        jwstoken.verify(key)
        appendToLog("OK")
    except Exception as e:
        appendToLog("Firma Invalida")